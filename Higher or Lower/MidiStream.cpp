﻿#include "targetver.h"
#include <windows.h>
#include <process.h>
#include <tchar.h>

#include <stdlib.h>

#ifdef MEMWATCH
#include "memwatch/memwatch.h"
#endif

#include "MidiStream.h"

#define TICKS_PER_BEAT 1
#define BEATS_TO_TICKS( x ) ( ( x ) * TICKS_PER_BEAT )

#define USERDATA_NONE 0
#define USERDATA_CALLBACK 1

MidiStream::MidiStream():
	midi( NULL ),
	error( MIDI_STREAM_ERROR_NOERROR )
{
	error_buffer[ 0 ] = _T( '\0' );
}

MidiStream::MidiStream( const WORD volume, const int tempo ):
	midi( NULL ),
	error( MIDI_STREAM_ERROR_NOERROR )
{
	error_buffer[ 0 ] = _T( '\0' );
	open( volume, tempo );
}

MidiStream::~MidiStream()
{
	close();
}

/*
	Child thread function. This thread is used to free buffers.
*/
unsigned __stdcall MidiStream::thread( void *p )
{
	MSG msg;
	BOOL ret;
	
	for( ;; )
	{
		ret = GetMessage( &msg, ( HWND )-1, 0, 0 );
		if( ret == 0 || ret == -1 )
			return 0;

		switch( msg.message )
		{
			case MIDI_THREAD_MESSAGE_FREE:
				if( ( ( ( LPMIDIHDR )msg.lParam )->dwFlags & MHDR_PREPARED ) == MHDR_PREPARED )
					while( midiOutUnprepareHeader( ( HMIDIOUT )msg.wParam, ( LPMIDIHDR )msg.lParam, sizeof( *( LPMIDIHDR )msg.lParam ) ) != MMSYSERR_NOERROR )
						Sleep( 1 );
				free( ( DWORD * )( ( ( LPMIDIHDR )msg.lParam )->lpData ) );
				free( ( LPMIDIHDR )msg.lParam );
				break;
			case MIDI_THREAD_MESSAGE_QUIT:
				return 0;
				break;
			default:
				break;
		}
	}
}

/*
	Callback function to receive information about the state of the MIDI stream.
*/
void CALLBACK MidiStream::callback( HMIDIOUT hmo, UINT wMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2 )
{
	switch( wMsg )
	{
		case MOM_OPEN:
			break;
		case MOM_CLOSE:
			break;
		case MOM_DONE:
		{
			LPMIDIHDR header = ( LPMIDIHDR )dwParam1;
			CallbackStruct *callback = ( CallbackStruct * )header->dwUser;

			if( callback != NULL )
			{
				callback->callback( callback->data );
				free( callback );
			}
			
			PostThreadMessage( ( ( MidiStream * )dwInstance )->thread_id, MIDI_THREAD_MESSAGE_FREE, ( WPARAM )hmo, ( LPARAM )header );
			break;
		}
		case MOM_POSITIONCB:
		{
//			LPMIDIHDR header = ( LPMIDIHDR )dwParam1;
			break;
		}
		default:
			break;
	}
}

/*
	Opens a Microsoft MIDI Mapper stream.
*/
int MidiStream::open( const WORD volume, const int tempo )
{
	UINT device_id;
	MMRESULT result;
	MIDIPROPTIMEDIV timediv;
	
	if( midi != NULL )
		close();

	device_id = MIDI_MAPPER;
	if( ( result = midiStreamOpen( &midi, &device_id, 1, ( DWORD_PTR )callback, ( DWORD_PTR )this, CALLBACK_FUNCTION ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		midi = NULL;
		return 0;
	}
	
	//Specify the amount of ticks per beat to use in the MIDI stream.
	//This property is specified using a 16-bit value, and the highest order bit
	//must be zero to indicate that the property specifies ticks per beat.
	timediv.cbStruct = sizeof( timediv );
	timediv.dwTimeDiv = TICKS_PER_BEAT & 0x00007FFF;
	if( ( result = midiStreamProperty( midi, ( LPBYTE )&timediv, MIDIPROP_SET | MIDIPROP_TIMEDIV ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		midiStreamClose( midi );
		midi = NULL;
		return 0;
	}

	set_volume( volume );

	if( !set_tempo( tempo ) )
	{
		midiStreamClose( midi );
		midi = NULL;
		return 0;
	}
	
	if( ( thread_handle = ( HANDLE )_beginthreadex( NULL, 0, thread, NULL, 0, &thread_id ) ) == NULL )
	{
		error = MIDI_STREAM_ERROR_THREAD;
		midiStreamClose( midi );
		midi = NULL;
		return 0;
	}
	
	//Wait for the thread's message queue to be created.
	while( !PostThreadMessage( thread_id, WM_NULL, 0, 0 ) )
		Sleep( 1 );

	return 1;
}

/*
	Returns true if the MIDI device supports volume changes.
*/
int MidiStream::supports_volume()
{
	MIDIOUTCAPS caps;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}

	if( midiOutGetDevCaps( MIDI_MAPPER, &caps, sizeof( caps ) ) != MMSYSERR_NOERROR )
		return 0;
	
	if( ( caps.dwSupport & MIDICAPS_VOLUME ) != MIDICAPS_VOLUME )
		return 0;

	return 1;
}

/*
	Closes the MIDI stream.
*/
void MidiStream::close()
{
	if( midi == NULL )
		return;
	
	midiStreamStop( midi );
	if( PostThreadMessage( thread_id, MIDI_THREAD_MESSAGE_QUIT, 0, 0 ) )
		WaitForSingleObject( thread_handle, INFINITE );
	CloseHandle( thread_handle );
	midiStreamClose( midi );
	midi = NULL;
}

/*
	Changes the overall volume of the MIDI stream.
*/
int MidiStream::set_volume( const WORD volume )
{
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}
	
	if( ( result = midiOutSetVolume( ( HMIDIOUT )midi, ( DWORD )volume | ( DWORD )volume << 16 ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Sets the tempo of the MIDI stream.
*/
int MidiStream::set_tempo( const DWORD bpm )
{
	MIDIPROPTEMPO tempo;
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}
	
	//Tempo is specified in microseconds per quarter note (or per beat).
	tempo.cbStruct = sizeof( tempo );
	tempo.dwTempo = 60000000UL / ( DWORD )bpm;
	if( ( result = midiStreamProperty( midi, ( LPBYTE )&tempo, MIDIPROP_SET | MIDIPROP_TEMPO ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Resumes the MIDI stream for playback.
*/
int MidiStream::play()
{
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}

	if( ( result = midiStreamRestart( midi ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Pauses the MIDI stream's playback.
*/
int MidiStream::pause()
{
	MMRESULT result;
	
	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}

	if( ( result = midiStreamPause( midi ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Stops the MIDI stream.
*/
int MidiStream::stop()
{
	MMRESULT result;
	
	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}

	if( ( result = midiStreamStop( midi ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Immediately sends a short message to the MIDI stream.
*/
int MidiStream::send_short_message( const DWORD msg )
{
	MMRESULT result;
	
	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}

	if( ( result = midiOutShortMsg( ( HMIDIOUT )midi, msg ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	return 1;
}

/*
	Queues a short message in the MIDI stream.
*/
int MidiStream::queue_short_message( const DWORD delta_beats, const DWORD msg )
{
	MIDIHDR *header;
	MIDIEVENT *event;
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}
	
	if( ( header = ( MIDIHDR * )malloc( sizeof( *header ) ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	if( ( event = ( MIDIEVENT * )malloc( sizeof( *event ) ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		free( header );
		return 0;
	}

	event->dwDeltaTime = BEATS_TO_TICKS( delta_beats );
	event->dwStreamID = 0;
	event->dwEvent = MEVT_F_SHORT | MEVT_SHORTMSG << 24 | ( msg & 0x00FFFFFF );
	
	header->lpData = ( LPSTR )event;
	header->dwBufferLength = sizeof( *event ) - sizeof( event->dwParms );
	header->dwBytesRecorded = sizeof( *event ) - sizeof( event->dwParms );
	header->dwUser = USERDATA_NONE << 16;
	header->dwFlags = 0;
	
	if( ( result = midiOutPrepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		free( header );
		free( event );
		return 0;
	}

	if( ( result = midiStreamOut( midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		midiOutUnprepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) );
		free( header );
		free( event );
		return 0;
	}
	
	return 1;
}

/*
	Queues a note in the MIDI stream.
*/
int MidiStream::queue_note( const char note, const char velocity, const DWORD beats, void ( *const callback )( void * ), void *const data )
{
	MIDIHDR *header;
	DWORD *event;
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}
	
	if( ( header = ( MIDIHDR * )malloc( sizeof( *header ) ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	if( ( event = ( DWORD * )malloc( sizeof( DWORD ) * 6 ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		free( header );
		return 0;
	}

	event[ 0 ] = 0;
	event[ 1 ] = 0;
	event[ 2 ] = MEVT_F_SHORT | MEVT_SHORTMSG << 24 | ( MIDI_MSG_NOTE_ON( note, velocity ) & 0x00FFFFFF );
	event[ 3 ] = BEATS_TO_TICKS( beats );
	event[ 4 ] = 0;
	event[ 5 ] = MEVT_F_SHORT | MEVT_SHORTMSG << 24 | ( MIDI_MSG_NOTE_OFF( note, 64 ) & 0x00FFFFFF );

	header->lpData = ( LPSTR )event;
	header->dwBufferLength = sizeof( DWORD ) * 6;
	header->dwBytesRecorded = sizeof( DWORD ) * 6;
	header->dwFlags = 0;
	header->dwOffset = 0;	//The documentation says this only needs to be set when the callback flag is specified.
	
	if( callback != NULL )
	{
		if( ( header->dwUser = ( DWORD_PTR )malloc( sizeof( CallbackStruct ) ) ) == NULL )
		{
			midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
			error = MIDI_STREAM_ERROR_MIDI;
			free( header );
			free( event );
			return 0;
		}

		( ( CallbackStruct * )header->dwUser )->callback = callback;
		( ( CallbackStruct * )header->dwUser )->data = data;
	}
	else
		header->dwUser = ( DWORD_PTR )NULL;
	
	
	if( ( result = midiOutPrepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		if( ( CallbackStruct * )header->dwUser != NULL )
			free( ( CallbackStruct * )header->dwUser );
		free( header );
		free( event );
		return 0;
	}

	if( ( result = midiStreamOut( midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		midiOutUnprepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) );
		if( ( CallbackStruct * )header->dwUser != NULL )
			free( ( CallbackStruct * )header->dwUser );
		free( header );
		free( event );
		return 0;
	}
	
	return 1;
}

/*
	Queues a note in the MIDI stream.
*/
int MidiStream::queue_rest( const char note, const char velocity, const DWORD beats, void ( *const callback )( void * ), void *const data )
{
	MIDIHDR *header;
	DWORD *event;
	MMRESULT result;

	if( midi == NULL )
	{
		error = MIDI_STREAM_ERROR_NOTOPEN;
		return 0;
	}
	
	if( ( header = ( MIDIHDR * )malloc( sizeof( *header ) ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		return 0;
	}

	if( ( event = ( DWORD * )malloc( sizeof( DWORD ) * 6 ) ) == NULL )
	{
		midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		free( header );
		return 0;
	}

	event[ 0 ] = 0;
	event[ 1 ] = 0;
	event[ 2 ] = MEVT_F_SHORT | MEVT_SHORTMSG << 24 | ( MIDI_MSG_NOTE_OFF( note, velocity ) & 0x00FFFFFF );
	event[ 3 ] = BEATS_TO_TICKS( beats );
	event[ 4 ] = 0;
	event[ 5 ] = MEVT_F_SHORT | MEVT_NOP << 24;

	header->lpData = ( LPSTR )event;
	header->dwBufferLength = sizeof( DWORD ) * 6;
	header->dwBytesRecorded = sizeof( DWORD ) * 6;
	header->dwFlags = 0;
	header->dwOffset = 0;
	
	if( callback != NULL )
	{
		if( ( header->dwUser = ( DWORD_PTR )malloc( sizeof( CallbackStruct ) ) ) == NULL )
		{
			midiOutGetErrorText( MMSYSERR_NOMEM, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
			error = MIDI_STREAM_ERROR_MIDI;
			free( header );
			free( event );
			return 0;
		}

		( ( CallbackStruct * )header->dwUser )->callback = callback;
		( ( CallbackStruct * )header->dwUser )->data = data;
	}
	else
		header->dwUser = ( DWORD_PTR )NULL;
	
	
	if( ( result = midiOutPrepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		if( ( CallbackStruct * )header->dwUser != NULL )
			free( ( CallbackStruct * )header->dwUser );
		free( header );
		free( event );
		return 0;
	}

	if( ( result = midiStreamOut( midi, header, sizeof( *header ) ) ) != MMSYSERR_NOERROR )
	{
		midiOutGetErrorText( result, error_buffer, sizeof( error_buffer ) / sizeof( *error_buffer ) );
		error = MIDI_STREAM_ERROR_MIDI;
		midiOutUnprepareHeader( ( HMIDIOUT )midi, header, sizeof( *header ) );
		if( ( CallbackStruct * )header->dwUser != NULL )
			free( ( CallbackStruct * )header->dwUser );
		free( header );
		free( event );
		return 0;
	}
	
	return 1;
}

const TCHAR *MidiStream::get_error_text()
{
	switch( error )
	{
		default:
		case MIDI_STREAM_ERROR_NOERROR:
			_tcsncpy( error_buffer, _T( "No error." ), sizeof( error_buffer ) / sizeof( *error_buffer ) );
			break;
		case MIDI_STREAM_ERROR_MIDI:
			break;
		case MIDI_STREAM_ERROR_NOTOPEN:
			_tcsncpy( error_buffer, _T( "The MIDI stream is not open." ), sizeof( error_buffer ) / sizeof( *error_buffer ) );
			break;
		case MIDI_STREAM_ERROR_THREAD:
			_tcsncpy( error_buffer, _T( "Failed to create thread." ), sizeof( error_buffer ) / sizeof( *error_buffer ) );
			break;
	}
	
	error = MIDI_STREAM_ERROR_NOERROR;
	error_buffer[ sizeof( error_buffer ) / sizeof( *error_buffer ) - 1 ] = _T( '\0' );
	return error_buffer;
}

#include <stdio.h>

char MidiStream::scientific_to_midi_note( const TCHAR *s )
{
	enum Note note;
	int offset = 0;
	int octave;
	int midi_note;

	//Skip leading whitespace.
	for( ; _istspace( *s ); ++s );

	//Determine the note (ignoring accidentals).
	switch( _totlower( *s ) )
	{
		case _T( 'a' ):
			note = A;
			break;
		case _T( 'b' ):
			note = B;
			break;
		case _T( 'c' ):
			note = C;
			break;
		case _T( 'd' ):
			note = D;
			break;
		case _T( 'e' ):
			note = E;
			break;
		case _T( 'f' ):
			note = F;
			break;
		case _T( 'g' ):
			note = G;
			break;
		default:
			return -1;
	}

	//Determine the accidentals.
	for( ;; ++s )
		switch( *s )
		{
			case _T( '#' ):
			case _T( '♯' ):
				++offset;
				break;
			case _T( 'b' ):
			case _T( '♭' ):
				--offset;
				break;
			default:
				goto no_more_accidentals;
		}
	no_more_accidentals:

	//Determine the octave number.
	if( _stscanf_s( s, _T( "%d" ), &octave ) == EOF )
		return -1;

	//Convert the note to a midi note number.
	midi_note = note + octave * 12 + offset;
	if( midi_note < 0 || midi_note > 127 )
		return -1;

	return midi_note;
}
