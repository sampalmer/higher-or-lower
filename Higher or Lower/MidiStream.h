#ifndef _MIDISTREAM_H_
#define _MIDISTREAM_H_

#include "targetver.h"
#include <windows.h>
#include <tchar.h>

//MIDI Messages
#define MIDI_MSG_PROGRAM_CHANGE( patch ) ( 0xC0 | ( ( patch ) << 8 ) )
#define MIDI_MSG_NOTE_ON( note, velocity ) ( 0x90 | ( ( note ) << 8 ) | ( ( velocity ) << 16 ) )
#define MIDI_MSG_NOTE_OFF( note, velocity ) ( 0x80 | ( ( note ) << 8 ) | ( ( velocity ) << 16 ) )

//Patches
//http://www.midi.org/techspecs/gm1sound.php
#define MIDI_PROGRAM_PIANO 0
#define MIDI_PROGRAM_GUITAR 25
#define MIDI_PROGRAM_BASS 33

//Range of 88-key piano: A0 - C8 = 21 - 108
//Range of 19-fret 6-string guitar: E2 - B5 = 40 - 83
//Range of 19-fret 4-string bass: E1 - D4 = 28 - 62
//Range of 19-fret 5-string bass: B0 - D4 = 23 - 62
//Range of MIDI: C-1 - G9 = 0 - 127

class MidiStream
{
private:
	HMIDISTRM midi;
	TCHAR error_buffer[ MAXERRORLENGTH ];
	enum
	{
		MIDI_STREAM_ERROR_NOERROR,
		MIDI_STREAM_ERROR_MIDI,
		MIDI_STREAM_ERROR_NOTOPEN,
		MIDI_STREAM_ERROR_THREAD
	} error;
	HANDLE thread_handle;
	unsigned thread_id;
	
	enum
	{
		MIDI_THREAD_MESSAGE_FREE = WM_APP,
		MIDI_THREAD_MESSAGE_QUIT
	};

	typedef struct
	{
		void ( *callback )( void * );
		void *data;
	} CallbackStruct;

	static unsigned __stdcall thread( void * );
	static void CALLBACK callback( HMIDIOUT hmo, UINT wMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2 );

public:
	enum
	{
		C = 12,		//Midi note for C0
		C_sharp,
		D,
		D_sharp,
		E,
		F,
		F_sharp,
		G,
		G_sharp,
		A,
		A_sharp,
		B
	} Note;

	MidiStream();
	MidiStream( const WORD volume, const int tempo );
	~MidiStream();
	int open( const WORD volume, const int tempo );
	int supports_volume();
	void close();
	int set_volume( const WORD volume );
	int set_tempo( const DWORD bpm );

	int play();
	int pause();
	int stop();
	int send_short_message( const DWORD msg );
	int queue_short_message( const DWORD delta_time, const DWORD msg );
	int queue_note( const char note, const char velocity, const DWORD beats, void ( *const callback )( void * ), void *const data );
	int queue_rest( const char note, const char velocity, const DWORD beats, void ( *const callback )( void * ), void *const data );
	const TCHAR *get_error_text();

	static char scientific_to_midi_note( const TCHAR *const note );
};

#endif
