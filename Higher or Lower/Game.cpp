#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include "Game.h"

#ifdef MEMWATCH
#include "memwatch/memwatch.h"
#endif

#define VOLUME_TO_MIDI_VOLUME( x )( ( WORD )( x ) * 65535 / VOLUME_MAX )

Game::Game():
	error( GAME_ERROR_NOERROR ),
	state( GAME_STATE_NOTINITIALISED ),
	previous_state( GAME_STATE_NOTINITIALISED )
{
}

Game::Game( const WORD volume, const int tempo, void ( *const callback )( GameState state ) ):
	error( GAME_ERROR_NOERROR ),
	previous_state( GAME_STATE_NOTINITIALISED )

{
	initialise( volume, tempo, callback );
}

Game::~Game()
{
}

/*
	Sets the current game state and calls the callback function to notify the owner of the object.
*/
void Game::set_state( const GameState state )
{
	this->previous_state = this->state;
	this->state = state;
	callback( state );
}

/*
	Initialises the game for play.
*/
int Game::initialise( const WORD volume, const int tempo, void ( *const callback )( GameState state ) )
{
	srand( ( unsigned int )time( NULL ) );
	
	this->callback = callback;

	if( !midi.open( VOLUME_TO_MIDI_VOLUME( volume ), tempo ) )
	{
		error = GAME_ERROR_MIDISTREAM;
		set_state( GAME_STATE_NOTINITIALISED );
		return 0;
	}

	set_state( GAME_STATE_NOTRUNNING );
	return 1;
}

/*
	Returns true if the MIDI stream supports volume changes.
*/
int Game::supports_volume()
{
	return midi.supports_volume();
}

/*
	Sets the current volume of the MIDI stream.
*/
int Game::set_volume( const WORD volume )
{
	return midi.set_volume( VOLUME_TO_MIDI_VOLUME( volume ) );
}

/*
	Sets the current tempo of the MIDI stream.
*/
int Game::set_tempo( const DWORD bpm )
{
	return midi.set_tempo( bpm );
}

/*
	Generates two MIDI notes to play based on the current difficulty setting.
*/
void Game::generate_notes()
{
	char instrument_lower_limit, instrument_upper_limit;
	char instrument_range;
	char lower_limit, upper_limit;
	char range;

	//Range of 88-key piano: A0 - C8 = 21 - 108
	//Range of 19-fret guitar: E2 - B5 = 40 - 83
	//Range of 19-fret bass: E1 - D4 = 28 - 62
	switch( instrument )
	{
		default:
		case GAME_INSTRUMENT_PIANO:
			instrument_lower_limit = 21;
			instrument_upper_limit = 108;
			break;
		case GAME_INSTRUMENT_GUITAR:
			instrument_lower_limit = 40;
			instrument_upper_limit = 83;
			break;
		case GAME_INSTRUMENT_BASS:
			instrument_lower_limit = 28;
			instrument_upper_limit = 62;
			break;
	}
	
	instrument_range = instrument_upper_limit - instrument_lower_limit;
	range = instrument_range * ( 127 - difficulty ) / 127;

	//Ensure that the notes aren't always equal on the harder difficulty settings.
	if( range == 0 )
		range = 1;

	lower_limit = rand() % ( instrument_range - range + 1 ) + instrument_lower_limit;
	upper_limit = lower_limit + range;

	note_a = ( rand() % ( upper_limit - lower_limit + 1 ) + lower_limit ) << 8;
	note_b = ( rand() % ( upper_limit - lower_limit + 1 ) + lower_limit ) << 8;

	/*
	note_a = rand() % ( ( upper_limit << 8 ) - ( lower_limit << 8 ) + 1 ) + ( lower_limit << 8 );
	note_b = rand() % ( ( upper_limit << 8 ) - ( lower_limit << 8 ) + 1 ) + ( lower_limit << 8 );
	*/
}

/*
	Callback function to be called by the MIDI stream object when note b has played.
*/
void Game::note_played( void *pointer )
{
	Game *game = ( Game * )pointer;

	game->midi.stop();
	game->set_state( GAME_STATE_AWAITINGINPUT );
}

/*
	Plays the current two MIDI notes.
*/
int Game::play_notes()
{
	char instrument;

	assert( state == GAME_STATE_NOTRUNNING || state == GAME_STATE_AWAITINGINPUT );

	switch( this->instrument )
	{
		default:
		case GAME_INSTRUMENT_PIANO:
			instrument = MIDI_PROGRAM_PIANO;
			break;
		case GAME_INSTRUMENT_GUITAR:
			instrument = MIDI_PROGRAM_GUITAR;
			break;
		case GAME_INSTRUMENT_BASS:
			instrument = MIDI_PROGRAM_BASS;
			break;
	}
	
	if( !midi.queue_short_message( 0, MIDI_MSG_PROGRAM_CHANGE( instrument ) ) )
	{
		error = GAME_ERROR_MIDISTREAM;
		return 0;
	}
	if( !midi.queue_note( ( note_a & 0xFF00 ) >> 8, 127, 1, NULL, NULL ) )
	{
		error = GAME_ERROR_MIDISTREAM;
		return 0;
	}
	if( !midi.queue_note( ( note_b & 0xFF00 ) >> 8, 127, 1, note_played, this ) )
	{
		error = GAME_ERROR_MIDISTREAM;
		return 0;
	}
	if( !midi.play() )
	{
		error = GAME_ERROR_MIDISTREAM;
		return 0;
	}

	set_state( GAME_STATE_PLAYINGNOTES );
	return 1;
}

/*
	Starts the game. This causes the game to generate and play two notes in succession.
*/
int Game::start( const GameInstrument instrument, const char difficulty )
{
	assert( state == GAME_STATE_NOTRUNNING || state == GAME_STATE_AWAITINGINPUT );
	
	this->instrument = instrument;
	this->difficulty = difficulty;
	generate_notes();
	return play_notes();
}

/*
	Repeats the last two notes that were played.
*/
int Game::repeat()
{
	assert( state == GAME_STATE_AWAITINGINPUT );
		
	return play_notes();
}

/*
	Stops the game. This causes the MIDI stream to stop.
*/
int Game::stop()
{
	assert( state != GAME_STATE_NOTRUNNING );
	
	if( !midi.stop() )
	{
		error = GAME_ERROR_MIDISTREAM;
		return 0;
	}

	set_state( GAME_STATE_NOTRUNNING );
	return 1;
}

/*
	Guesses if the given condition is true. Proceeds to the next two notes on success.
*/
int Game::guess( const BOOL condition, const GameInstrument instrument, const char difficulty, BOOL *const correct )
{
	assert( state == GAME_STATE_AWAITINGINPUT );

	if( condition )
	{
		if( !start( instrument, difficulty ) )
			return 0;
		*correct = TRUE;
	}
	else
		*correct = FALSE;
	
	return 1;
}

/*
	Guesses if the second note is lower than the first. Proceeds to the next two notes on success.
*/
int Game::guess_lower( const GameInstrument instrument, const char difficulty, BOOL *const correct )
{
	assert( state == GAME_STATE_AWAITINGINPUT );

	return guess( note_b < note_a, instrument, difficulty, correct );
}

/*
	Guesses if the second note is equal to the first. Proceeds to the next two notes on success.
*/
int Game::guess_equal( const GameInstrument instrument, const char difficulty, BOOL *const correct )
{
	assert( state == GAME_STATE_AWAITINGINPUT );

	return guess( note_b == note_a, instrument, difficulty, correct );
}

/*
	Guesses if the second note is higher than the first. Proceeds to the next two notes on success.
*/
int Game::guess_higher( const GameInstrument instrument, const char difficulty, BOOL *const correct )
{
	assert( state == GAME_STATE_AWAITINGINPUT );

	return guess( note_b > note_a, instrument, difficulty, correct );
}

/*
	Returns the MIDI error text.
*/
const TCHAR *Game::get_error_text()
{
	switch( error )
	{
		default:
		case GAME_ERROR_NOERROR:
			_tcsncpy( error_buffer, _T( "No error." ), sizeof( error_buffer ) / sizeof( *error_buffer ) );
			break;
		case GAME_ERROR_MIDISTREAM:
			return midi.get_error_text();
		case GAME_ERROR_WRONGSTATE:
			_tcsncpy( error_buffer, _T( "This action cannot be performed while the game is in its current state." ), sizeof( error_buffer ) / sizeof( *error_buffer ) );
			break;
	}
	
	error = GAME_ERROR_NOERROR;
	error_buffer[ sizeof( error_buffer ) / sizeof( *error_buffer ) - 1 ] = _T( '\0' );
	return error_buffer;
}
