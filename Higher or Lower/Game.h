#ifndef _GAME_H_
#define _GAME_H_

#include "MidiStream.h"

#define DIFFICULTY_MIN 0
#define DIFFICULTY_MAX 127

#define VOLUME_MIN 0
#define VOLUME_MAX 63

#define TEMPO_MIN 20
#define TEMPO_MAX 208

typedef enum
{
	GAME_STATE_NOTINITIALISED,
	GAME_STATE_NOTRUNNING,
	GAME_STATE_PLAYINGNOTES,
	GAME_STATE_AWAITINGINPUT
} GameState;

typedef enum
{
	GAME_INSTRUMENT_PIANO,
	GAME_INSTRUMENT_GUITAR,
	GAME_INSTRUMENT_BASS
} GameInstrument;

class Game
{
private:
	MidiStream midi;
	void ( *callback )( GameState state );
	GameInstrument instrument;
	char difficulty;
	WORD note_a, note_b;
	TCHAR error_buffer[ 256 ];
	enum
	{
		GAME_ERROR_NOERROR,
		GAME_ERROR_MIDISTREAM,
		GAME_ERROR_WRONGSTATE
	} error;

	void generate_notes();
	static void note_played( void *pointer );
	int play_notes();
	int guess( const BOOL condition, const GameInstrument instrument, const char difficulty, BOOL *const correct );

public:
	GameState state, previous_state;
	
	Game();
	Game( const WORD volume, const int tempo, void ( *const callback )( GameState state ) );
	~Game();
	void set_state( const GameState state );
	int initialise( const WORD volume, const int tempo, void ( *const callback )( GameState state ) );
	int supports_volume();
	int set_volume( const WORD volume );
	int set_tempo( const DWORD bpm );
	int start( const GameInstrument instrument, const char difficulty );
	int repeat();
	int stop();
	int guess_lower( const GameInstrument instrument, const char difficulty, BOOL *const correct );
	int guess_equal( const GameInstrument instrument, const char difficulty, BOOL *const correct );
	int guess_higher( const GameInstrument instrument, const char difficulty, BOOL *const correct );
	const TCHAR *get_error_text();
};

#endif
