//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Higher or Lower.rc
//
#define IDI_HIGHERORLOWER               107
#define IDI_SMALL                       108
#define IDD_DIALOG                      129
#define IDC_START                       1002
#define IDC_STOP                        1004
#define IDC_HIGHER                      1005
#define IDC_LOWER                       1006
#define IDC_EQUAL                       1007
#define IDC_STATUS                      1008
#define IDC_PIANO                       1010
#define IDC_GUITAR                      1011
#define IDC_BASS                        1012
#define IDC_DIFFICULTY                  1015
#define IDC_VOLUME                      1017
#define IDC_REPEAT                      1018
#define IDC_VOLUME2                     1019
#define IDC_TEMPO                       1019
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
