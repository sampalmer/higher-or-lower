//Enable visual styles.
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <stdlib.h>

#include "targetver.h"
#include <windows.h>
#include <commctrl.h>
#include <tchar.h>

#ifdef MEMWATCH
#include "memwatch/memwatch.h"
#endif

#include "Game.h"
#include "resource.h"

#define DIFFICULTY_DEFAULT 64
#define VOLUME_DEFAULT 48
#define TEMPO_DEFAULT 60
#define INVERT_VOLUME( x ) ( VOLUME_MIN + VOLUME_MAX - ( x ) )
#define INVERT_TEMPO( x ) ( TEMPO_MIN + TEMPO_MAX - ( x ) )

static Game *game = NULL;
static HWND dialog = NULL;

/*
	Sets the enabled state of a dialog item.
*/
static void enable_dialog_item( const int item, const BOOL enable )
{
	HWND control;

	if( ( control = GetDlgItem( dialog, item ) ) == NULL )
		return;

	EnableWindow( control, enable );
}

/*
	Alters the dialog and its controls based on the current game state.
*/
static void update_dialog( const GameState state )
{
	switch( state )
	{
		case GAME_STATE_NOTRUNNING:
			enable_dialog_item( IDC_START, TRUE );
			enable_dialog_item( IDC_REPEAT, FALSE );
			enable_dialog_item( IDC_STOP, FALSE );
			enable_dialog_item( IDC_LOWER, FALSE );
			enable_dialog_item( IDC_EQUAL, FALSE );
			enable_dialog_item( IDC_HIGHER, FALSE );
			SetDlgItemText( dialog, IDC_STATUS, _T( "Press the start button to begin." ) );
			break;
		case GAME_STATE_PLAYINGNOTES:
			enable_dialog_item( IDC_START, FALSE );
			enable_dialog_item( IDC_REPEAT, FALSE );
			enable_dialog_item( IDC_STOP, TRUE );
			enable_dialog_item( IDC_LOWER, FALSE );
			enable_dialog_item( IDC_EQUAL, FALSE );
			enable_dialog_item( IDC_HIGHER, FALSE );
			SetDlgItemText( dialog, IDC_STATUS, _T( "Please wait for the notes to play." ) );
			break;
		case GAME_STATE_AWAITINGINPUT:
			enable_dialog_item( IDC_START, FALSE );
			enable_dialog_item( IDC_REPEAT, TRUE );
			enable_dialog_item( IDC_STOP, TRUE );
			enable_dialog_item( IDC_LOWER, TRUE );
			enable_dialog_item( IDC_EQUAL, TRUE );
			enable_dialog_item( IDC_HIGHER, TRUE );
			SetDlgItemText( dialog, IDC_STATUS, _T( "Was the second note higher than, equal to, or lower than the first?" ) );
			break;
		default:
			enable_dialog_item( IDC_START, FALSE );
			enable_dialog_item( IDC_REPEAT, FALSE );
			enable_dialog_item( IDC_STOP, FALSE );
			enable_dialog_item( IDC_LOWER, FALSE );
			enable_dialog_item( IDC_EQUAL, FALSE );
			enable_dialog_item( IDC_HIGHER, FALSE );
			break;
	}
}

/*
	Initialises the dialog box's controls and icons.
*/
static void initialise_dialog()
{
	CheckRadioButton( dialog, IDC_PIANO, IDC_BASS, IDC_PIANO );

	SendDlgItemMessage( dialog, IDC_DIFFICULTY, TBM_SETRANGEMIN, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )DIFFICULTY_MIN );
	SendDlgItemMessage( dialog, IDC_DIFFICULTY, TBM_SETRANGEMAX, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )DIFFICULTY_MAX );
	SendDlgItemMessage( dialog, IDC_DIFFICULTY, TBM_SETPOS, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )DIFFICULTY_DEFAULT );
	SendDlgItemMessage( dialog, IDC_VOLUME, TBM_SETRANGEMIN, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )VOLUME_MIN );
	SendDlgItemMessage( dialog, IDC_VOLUME, TBM_SETRANGEMAX, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )VOLUME_MAX );
	SendDlgItemMessage( dialog, IDC_VOLUME, TBM_SETPOS, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )INVERT_VOLUME( VOLUME_DEFAULT ) );
	SendDlgItemMessage( dialog, IDC_TEMPO, TBM_SETRANGEMIN, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )TEMPO_MIN );
	SendDlgItemMessage( dialog, IDC_TEMPO, TBM_SETRANGEMAX, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )TEMPO_MAX );
	SendDlgItemMessage( dialog, IDC_TEMPO, TBM_SETPOS, ( WPARAM )( BOOL )TRUE, ( LPARAM )( LONG )INVERT_TEMPO( TEMPO_DEFAULT ) );
	
	enable_dialog_item( IDC_VOLUME, game->supports_volume() );

	{
		HICON icon;
		icon = ( HICON )LoadImage( GetModuleHandle( NULL ), MAKEINTRESOURCE( IDI_HIGHERORLOWER ), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE );
		SendMessage( dialog, WM_SETICON, ICON_BIG, ( LPARAM )icon );
		icon = ( HICON )LoadImage( GetModuleHandle( NULL ), MAKEINTRESOURCE( IDI_SMALL ), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE );
		SendMessage( dialog, WM_SETICON, ICON_SMALL, ( LPARAM )icon );
	}
}

/*
	Determines the current MIDI instrument to use based on the user's selection in the GUI.
*/
static GameInstrument get_instrument()
{
	if( IsDlgButtonChecked( dialog, IDC_GUITAR ) == BST_CHECKED )
		return GAME_INSTRUMENT_GUITAR;
	
	if( IsDlgButtonChecked( dialog, IDC_BASS ) == BST_CHECKED )
		return GAME_INSTRUMENT_BASS;
	
	return GAME_INSTRUMENT_PIANO;
}

/*
	Callback window procedure for the main dialog box.
*/
static INT_PTR CALLBACK DialogProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
		case WM_INITDIALOG:

			dialog = hwnd;
			ShowWindow( hwnd, ( int )lParam );

			game = new Game( VOLUME_DEFAULT, TEMPO_DEFAULT, update_dialog );
			
			if( game->state == GAME_STATE_NOTINITIALISED )
			{
				MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
				EndDialog( hwnd, ( INT_PTR )EXIT_FAILURE );
				break;
			}

			initialise_dialog();
			break;
		case WM_VSCROLL:
		{
			HWND control;

			if( ( control = GetDlgItem( hwnd, IDC_VOLUME ) ) != NULL )
				if( ( HWND )lParam == control )
				{
					if( !game->set_volume( INVERT_VOLUME( ( WORD )SendMessage( control, TBM_GETPOS, 0, 0 ) ) ) )
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
					break;
				}

			if( ( control = GetDlgItem( hwnd, IDC_TEMPO ) ) != NULL )
				if( ( HWND )lParam == control )
				{
					if( !game->set_tempo( INVERT_TEMPO( ( DWORD )SendMessage( control, TBM_GETPOS, 0, 0 ) ) ) )
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
					break;
				}

			return FALSE;
		}
		case WM_COMMAND:
			switch( LOWORD( wParam ) )
			{
				case IDC_START:
					if( !game->start( get_instrument(), ( char )( DWORD )SendDlgItemMessage( hwnd, IDC_DIFFICULTY, TBM_GETPOS, 0, 0 ) ) )
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
					break;
				case IDC_REPEAT:
					if( !game->repeat() )
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
					break;
				case IDC_STOP:
					if( !game->stop() )
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
					break;
				case IDC_LOWER:
				{
					BOOL correct;

					if( !game->guess_lower( get_instrument(), ( char )( DWORD )SendDlgItemMessage( hwnd, IDC_DIFFICULTY, TBM_GETPOS, 0, 0 ), &correct ) )
					{
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
						break;
					}

					if( correct )
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Correct!\r\nPlease wait for the notes to play." ) );
					else
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Incorrect!" ) );

					break;
				}
				case IDC_EQUAL:
				{
					BOOL correct;

					if( !game->guess_equal( get_instrument(), ( char )( DWORD )SendDlgItemMessage( hwnd, IDC_DIFFICULTY, TBM_GETPOS, 0, 0 ), &correct ) )
					{
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
						break;
					}

					if( correct )
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Correct!\r\nPlease wait for the notes to play." ) );
					else
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Incorrect!" ) );

					break;
				}
				case IDC_HIGHER:
				{
					BOOL correct;

					if( !game->guess_higher( get_instrument(), ( char )( DWORD )SendDlgItemMessage( hwnd, IDC_DIFFICULTY, TBM_GETPOS, 0, 0 ), &correct ) )
					{
						MessageBox( hwnd, game->get_error_text(), _T( "Error" ), MB_ICONERROR | MB_OK );
						break;
					}

					if( correct )
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Correct!\r\nPlease wait for the notes to play." ) );
					else
						SetDlgItemText( hwnd, IDC_STATUS, _T( "Incorrect!" ) );

					break;
				}
				case IDCANCEL:
				case IDOK:
					EndDialog( hwnd, ( INT_PTR )EXIT_SUCCESS );
					delete game;
					break;
				default:
					return FALSE;
			}
		default:
			return FALSE;
	}

	return TRUE;
}

/*
	Entry-point
*/
int APIENTRY _tWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

//TODO: Work out the range of notes that Microsoft MIDI Mapper plays.
//TODO: Test produced pitches against AP tuner using the microphone and speakers to ensure that they're correct.
//TODO: IMPLEMENT PITCH BEND SO THAT SUB-SEMITONE PITCHES CAN BE PRODUCED FOR EXTRA DIFFICULTY AND PRECISION.
//Change coding for volume support detection.
//Fix focus problems when you click on a button that disables itself.
//Make callback optional for all events.
	return ( int )DialogBoxParam( hInstance, MAKEINTRESOURCE( IDD_DIALOG ), NULL, DialogProc, ( LPARAM )nCmdShow );
}
